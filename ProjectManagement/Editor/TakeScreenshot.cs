﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Shadowsun.Tools
{
    public class TakeScreenshot
    {
        [MenuItem( "Tools/Shadowsun/Take Screenshot #F2", false, 751 )]
        static void Screenshot()
        {
            string fileName = Application.productName + " - " + DateTime.Now.ToString( "yyyy MM dd - hh.mm.ss.fff" ) + ".png";
            string path = "Capture/Screenshots";

            if (!Directory.Exists( path ))
            {
                Directory.CreateDirectory( path );
            }

            ScreenCapture.CaptureScreenshot( path + "/" + fileName );
        }
    }
}
