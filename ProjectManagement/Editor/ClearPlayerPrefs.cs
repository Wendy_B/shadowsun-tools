﻿using UnityEditor;
using UnityEngine;

namespace Shadowsun.Tools
{
    public class ClearPlayerPrefs
    {
        [MenuItem( "Tools/Shadowsun/PlayerPrefs/Clear Player Prefs", false, 251 )]
        private static void NewMenuOption()
        {
            PlayerPrefs.DeleteAll();
        }
    }
}

