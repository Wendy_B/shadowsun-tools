﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;

public class BuildManager : EditorWindow
{
    private enum e_platform
    {
        DESKTOP,
        STEAM
    }
    private enum e_buildState
    {
        PROTOTYPE,
        ALPHA,
        BETA,
        RELEASE
    }

    private Vector2 _scrollPos;

    private bool _clearPlayerPrefs = true;
    private bool _devEnabled; // Switch for Development build
    private bool _debugLogEnabled; // Switch to enable Debug.logs in build
    

    private bool _macEnabled; // Enable Mac OS build
    private bool _linux64Enabled; // Enable Linux 64 Build
    private bool _win32Enabled; // Enable win 32 Build
    private bool _win64Enabled; // Enable win 64 Build

    private string _buildSavePath;

    private bool _showProp; // Global Properties foldout 
    private bool _showProjectProp; // Project properties foldout 
    private bool _showGenericProp; // Generic properties foldout 
    private bool _showPlatformProp; // Project properties foldout 
    private bool _showBuildProp; // Build properties foldout 
    private bool _showSceneProp; // Scene Management foldout 

    private bool _enableMultiSceneBuild;

    private string _companyName;
    private string _projectName;
    private string _specialPackage;

    private int _releaseVersion;
    private int _projectVersion;
    private int _buildNumber;

    private float _buildProgress;
    private string _buildProgressText = "Not Building Right Now";

    private e_platform _targetPlatform;
    private e_buildState _buildState;

    private GUIStyle boldCenteredTitle;
    private GUIStyle boldFoldout;

    [MenuItem( "Tools/Shadowsun/Project/BuildManager" )]
    public static void ShowWindow()
    {
        EditorWindow window = GetWindow<BuildManager>( true, "Shadowsun's Build Manager", true );
        window.minSize = new Vector2( 500f, 500f );
    }

    private void OnEnable()
    {
        LoadProjectProperties();
        LoadBuildSettings();
    }

    private void OnGUI()
    {
        boldCenteredTitle = new GUIStyle( GUI.skin.label );
        boldCenteredTitle.alignment = TextAnchor.LowerCenter;
        boldCenteredTitle.fontStyle = FontStyle.Bold;
        boldCenteredTitle.fontSize = 34;

        boldFoldout = new GUIStyle( EditorStyles.foldout );
        boldFoldout.fontStyle = FontStyle.Bold;
        boldFoldout.fontSize = 12;

        _scrollPos = EditorGUILayout.BeginScrollView( _scrollPos );
        {
            DrawTitleSection();
            DrawProjectPropertiesSection();
            DrawGenericPropertiesSection();
            DrawPlatformPropertiesSection();
            DrawBuildSection();
            DrawBuildButtonSection();
        }
        EditorGUILayout.EndScrollView();

        if (GUI.changed)
        {

        }
    }

    #region Drawers

    private void DrawTitleSection()
    {
        EditorGUILayout.Space();

        GUILayout.Label( "Shadowsun's Build Manager", boldCenteredTitle );

        EditorGUILayout.Space();
    }
    private void DrawProjectPropertiesSection()
    {
        _showProjectProp = EditorGUILayout.Foldout( _showProjectProp, "Project properties", boldFoldout );
        if (_showProjectProp)
        {
            EditorGUILayout.Space();
            EditorGUI.indentLevel++;
            {
                _targetPlatform = (e_platform)EditorGUILayout.EnumPopup( "Target Platform", _targetPlatform, GUILayout.Width( 250 ) );

                EditorGUILayout.Space();

                _clearPlayerPrefs = EditorGUILayout.ToggleLeft( "Clear Player Prefs", _clearPlayerPrefs );
                _devEnabled = EditorGUILayout.ToggleLeft( "Development Build", _devEnabled );

                EditorGUI.BeginDisabledGroup( !_devEnabled );
                EditorGUI.indentLevel += 2;
                {
                    _debugLogEnabled = EditorGUILayout.Toggle( "Debug Log", _debugLogEnabled );
                }
                EditorGUI.indentLevel -= 2;
                EditorGUI.EndDisabledGroup();

                EditorGUILayout.Space();

                EditorGUI.BeginDisabledGroup( true );
                {
                    EditorGUILayout.TextField( "Bundle Identifier", PlayerSettings.applicationIdentifier );
                }
                EditorGUI.EndDisabledGroup();

                EditorGUILayout.Space();

                _companyName = EditorGUILayout.TextField( "Company Name", _companyName );

                EditorGUI.BeginDisabledGroup( true );
                EditorGUILayout.TextField( "Current Company", PlayerSettings.companyName );
                EditorGUI.EndDisabledGroup();

                EditorGUILayout.Space();

                _projectName = EditorGUILayout.TextField( "Project Name", _projectName );

                EditorGUI.BeginDisabledGroup( true );
                {
                    EditorGUILayout.TextField( "Current Project Name", PlayerSettings.productName );
                }
                EditorGUI.EndDisabledGroup();

                EditorGUILayout.Space();

                EditorGUILayout.BeginHorizontal();
                {
                    _buildState = (e_buildState)EditorGUILayout.EnumPopup( "Project Version", _buildState, GUILayout.Width( 250 ) );
                    EditorGUI.BeginDisabledGroup( _buildState != e_buildState.RELEASE );
                    {
                        _releaseVersion = EditorGUILayout.IntField( _releaseVersion, GUILayout.Width( 45 ) );

                        if (GUILayout.Button( "+", GUILayout.Width( 25 ) ))
                        {
                            UpReleaseVersion();
                        }
                    }
                    EditorGUI.EndDisabledGroup();

                    _projectVersion = EditorGUILayout.IntField( _projectVersion, GUILayout.Width( 45 ) );

                    if (GUILayout.Button( "+", GUILayout.Width( 25 ) ))
                    {
                        UpProjectVersion();
                    }
                }
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                {
                    EditorGUI.BeginDisabledGroup( true );
                    {
                        EditorGUILayout.IntField( "Current Build", _buildNumber, GUILayout.Width( 200 ) );
                    }
                    EditorGUI.EndDisabledGroup();

                    if (GUILayout.Button( "0", GUILayout.Width( 25 ) ))
                    {
                        _buildNumber = 0;
                    }
                }
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();
                {
                    _specialPackage = EditorGUILayout.TextField( "Special Build", _specialPackage );

                    if (GUILayout.Button( "0", GUILayout.Width( 25 ) ))
                    {
                        _specialPackage = "";
                    }

                    GUILayout.Space( 15 );
                }
                EditorGUILayout.EndHorizontal();

                EditorGUI.BeginDisabledGroup( true );
                {
                    EditorGUILayout.TextField( "Current Version", PlayerSettings.bundleVersion );
                }
                EditorGUI.EndDisabledGroup();

                EditorGUILayout.Space();

                // --- Buttons
                EditorGUILayout.BeginHorizontal();
                {
                    GUILayout.Space( 20 );
                    if (GUILayout.Button( "Default", GUILayout.Width( 100 ) ))
                    {
                        DefaultProjectProperties();
                    }
                    GUILayout.Space( 30 );

                    if (GUILayout.Button( "Reset", GUILayout.Width( 100 ) ))
                    {
                        LoadProjectProperties();
                    }

                    if (GUILayout.Button( "Save", GUILayout.Width( 100 ) ))
                    {
                        SaveProjectProperties();
                    }
                }
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.Space();
                EditorGUILayout.BeginHorizontal();
                {
                    GUILayout.Space( 150 );

                }
                EditorGUILayout.EndHorizontal();

            }
            EditorGUI.indentLevel--;
        } // end foldout
        EditorGUILayout.Space();


    }
    private void DrawGenericPropertiesSection()
    {
        _showGenericProp = EditorGUILayout.Foldout( _showGenericProp, "Generic properties", boldFoldout );
        if (_showGenericProp)
        {
            EditorGUILayout.Space();

            EditorGUI.BeginDisabledGroup( true );
            EditorGUI.indentLevel++;
            {
                EditorGUILayout.LabelField( "Coming Soon : Generic Prop" );
            }
            EditorGUI.indentLevel--;
            EditorGUI.EndDisabledGroup();
        }// end foldout
        EditorGUILayout.Space();
    }
    private void DrawPlatformPropertiesSection()
    {
        _showPlatformProp = EditorGUILayout.Foldout( _showPlatformProp, "Platform Specific properties", boldFoldout );
        if (_showPlatformProp)
        {
            EditorGUILayout.Space();

            EditorGUI.BeginDisabledGroup( true );
            EditorGUI.indentLevel++;
            {
                switch (_targetPlatform)
                {
                    case e_platform.DESKTOP:
                        EditorGUILayout.LabelField( "Coming Soon : Desktop" );
                        break;
                    default:
                        EditorGUILayout.LabelField( "Coming Soon" );
                        break;
                }
            }
            EditorGUI.indentLevel--;
            EditorGUI.EndDisabledGroup();
        }// end foldout
        EditorGUILayout.Space();
    }
   
    private void DrawBuildSection()
    {
        _showBuildProp = EditorGUILayout.Foldout( _showBuildProp, "Build Properties", boldFoldout );
        if (_showBuildProp)
        {
            EditorGUILayout.Space();

            EditorGUI.indentLevel++;
            {
                switch (_targetPlatform)
                {
                    case e_platform.DESKTOP:
                        EditorGUILayout.BeginHorizontal();
                    {
                        _macEnabled = EditorGUILayout.ToggleLeft( "MAC OSX", _macEnabled, GUILayout.Width( 90 ) );
                            _linux64Enabled = EditorGUILayout.ToggleLeft( "Linux 64", _linux64Enabled, GUILayout.Width( 90 ) );
                            _win32Enabled = EditorGUILayout.ToggleLeft( "Win32", _win32Enabled, GUILayout.Width( 90 ) );
                            _win64Enabled = EditorGUILayout.ToggleLeft( "Win64", _win64Enabled, GUILayout.Width( 90 ) );
                        }
                        EditorGUILayout.EndHorizontal();
                        break;
                    default:
                        EditorGUILayout.LabelField( "Coming Soon" );
                        break;
                } // End Switch

                EditorGUILayout.Space();

                EditorGUILayout.BeginHorizontal();
                {
                    _buildSavePath = EditorGUILayout.TextField( "Build path : ", _buildSavePath );
                    if (GUILayout.Button( "...", GUILayout.Width( 25 ) ))
                    {
                        ChangeBuildPath();
                    }
                    GUILayout.Space( 15 );
                }
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.BeginHorizontal();
                {
                    GUILayout.Space( 20 );
                    if (GUILayout.Button( "Open Folder", GUILayout.Width( 100 ) ))
                    {
                        ShowInExplorer();
                    }

                }
                EditorGUILayout.EndHorizontal();

                EditorGUILayout.Space();
            }
            EditorGUI.indentLevel--;
        }// end foldout
        EditorGUILayout.Space();
    }
    private void DrawBuildButtonSection()
    {
        EditorGUILayout.Space();

        EditorGUILayout.BeginHorizontal();
        {
            GUILayout.Space( 20 );
            if (GUILayout.Button( "Build Now!", GUILayout.Width( 100 ) ))
            {
                BuildGame();
            }
            GUILayout.Space( 15 );
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();

        EditorGUILayout.BeginHorizontal();
        {
            GUILayout.Space( 15 );
            EditorGUI.ProgressBar( GUILayoutUtility.GetRect( 100, 20 ), _buildProgress, _buildProgressText );
            GUILayout.Space( 15 );
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();
        EditorGUILayout.Space();
    }

    #endregion

    #region MainMethods

    private void DefaultProjectProperties()
    {
        _companyName = "DefaultCompany";
        _projectName = "NewProject";
        _buildState = e_buildState.PROTOTYPE;
        _releaseVersion = 0;
        _projectVersion = 0;
        _buildNumber = 0;
        _specialPackage = "";

    }
    private void LoadProjectProperties()
    {
        _companyName = PlayerSettings.companyName;
        _projectName = PlayerSettings.productName.Replace( "_DEV", "" );

        string[] str = PlayerSettings.bundleVersion.Replace( "_DEV", "" ).Split( '.' );

        if (Int32.Parse( str[0] ) == 1)
        {
            _buildState = e_buildState.RELEASE;
            _releaseVersion = Int32.Parse( str[1] );
        }
        else
        {
            _buildState = (e_buildState)Int32.Parse( str[1] );
        }
        _projectVersion = Int32.Parse( str[2] );
        _buildNumber = Int32.Parse( str[3] );
        _specialPackage = str.Length == 5 ? str[4] : "";
    }
    private void SaveProjectProperties()
    {
        PlayerSettings.companyName = _companyName;
        PlayerSettings.productName = _devEnabled ? _projectName + "_DEV" : _projectName;
        PlayerSettings.bundleVersion = GenerateBundleVersion();

        PlayerSettings.applicationIdentifier = "com."
                                                + _companyName.Replace( " ", "" )
                                                + "."
                                                + _projectName.Replace( " ", "" )
                                                + ( _devEnabled ? "_DEV" : "" );
    }

    private void LoadBuildSettings()
    {
        if (string.IsNullOrEmpty( _buildSavePath ))
        {
            _buildSavePath = Application.dataPath;
            _buildSavePath = _buildSavePath.Replace( "Assets", "Builds" );

            if (!Directory.Exists( _buildSavePath ))
            {
                Directory.CreateDirectory( _buildSavePath );
            }
        }
    }

    private void UpProjectVersion()
    {
        _projectVersion++;
    }
    private void UpReleaseVersion()
    {
        _releaseVersion++;
    }
    private string GenerateBundleVersion()
    {
        string version = "";
        if (_buildState == e_buildState.RELEASE)
        {
            version = "1." + _releaseVersion;
        }
        else
        {
            version = "0." + (int)_buildState;
        }
        version += "." + _projectVersion
                 + "." + _buildNumber
                 + ( _specialPackage != "" ? "." + _specialPackage : "" )
                 + ( _devEnabled ? "_DEV" : "" );
        return version;
    }

    private void ChangeBuildPath()
    {
        _buildSavePath = EditorUtility.OpenFolderPanel( "Path to save the build", "", "" );
    }

    private void BuildGame()
    {
        switch (_targetPlatform)
        {
            case e_platform.DESKTOP:
                DesktopBuildCheck();
                break;
            default:
                break;
        }
       
        _buildProgress = 0f;
        _buildProgressText = "Starting...";
        Repaint();
        
        _buildProgress = 0.02f;
        _buildProgressText = "Setting Build Version...";
        LoadProjectProperties();
        _buildNumber++;
        SaveProjectProperties();
        Repaint();
        
        if ( _clearPlayerPrefs )
        {
            _buildProgress = 0.04f;
            _buildProgressText = "Clear Player Prefs...";
            PlayerPrefs.DeleteAll();
            Repaint();
        }
        
        _buildProgress = 0.06f;
        _buildProgressText = "Preparing Scenes...";
        Repaint();
        string[] levels = EditorBuildSettingsScene.GetActiveSceneList( EditorBuildSettings.scenes );

        _buildProgress = 0.08f;
        _buildProgressText = "Checking Build Path...";
        Repaint();
        if (string.IsNullOrEmpty( _buildSavePath ))
        {
            _buildSavePath = Application.dataPath;
            _buildSavePath = _buildSavePath.Replace( "Assets", "Builds" );
        }

        if (_buildSavePath[_buildSavePath.Length - 1] == '/')
            _buildSavePath = _buildSavePath.Substring( 0, _buildSavePath.Length - 1 );

        if (!Directory.Exists( _buildSavePath ))
        {
            Directory.CreateDirectory( _buildSavePath );
        }

        _buildProgress = 0.1f;
        _buildProgressText = "Starting Build...";
        Repaint();
        switch (_targetPlatform)
        {
            case e_platform.DESKTOP:
                DesktopBuildGame( levels );
                break;
            default:
                break;
        }

        _buildProgress = 1f;
        _buildProgressText = "DONE!";
        Repaint();

        GUIUtility.ExitGUI();
    }

    private void DesktopBuildCheck()
    {
        if (!( _macEnabled || _linux64Enabled || _win32Enabled || _win64Enabled ))
        {
            _buildProgressText = "Please Check at least 1 platform";
            Repaint();
            return;
        }
    }
    private void DesktopBuildGame(string[] levels)
    {
        if (_macEnabled)
        {
            _buildProgressText = "Building for MAC OSX ...";
            Repaint();

            Debug.Log( "MAC OSX" +
               BuildPipeline.BuildPlayer(
                    levels, _buildSavePath + "/Build V" + PlayerSettings.bundleVersion + "/" + _projectName + " MAC OSX " + PlayerSettings.bundleVersion + "/" + _projectName,
                    BuildTarget.StandaloneOSX, _devEnabled ? BuildOptions.Development : BuildOptions.None
            ) );
        }

        _buildProgress = .25f;
        
        if (_linux64Enabled)
        {
            _buildProgressText = "Building for Linux64 ...";
            Repaint();

            Debug.Log( "Linux" +
              BuildPipeline.BuildPlayer(
                  levels, _buildSavePath + "/Build V" + PlayerSettings.bundleVersion + "/" + _projectName + " Linux64 " + PlayerSettings.bundleVersion + "/" + _projectName + ".x86_64",
                  BuildTarget.StandaloneLinux64, _devEnabled ? BuildOptions.Development : BuildOptions.None
           ) );
        }

        _buildProgress = .5f;

        if (_win32Enabled)
        {
            _buildProgressText = "Building for Win32 ...";
            Repaint();

            Debug.Log( "win32" +
               BuildPipeline.BuildPlayer(
                  levels, _buildSavePath + "/Build V" + PlayerSettings.bundleVersion + "/" + _projectName + " win32 " + PlayerSettings.bundleVersion + "/" + _projectName + ".exe",
                  BuildTarget.StandaloneWindows, _devEnabled ? BuildOptions.Development : BuildOptions.None
             ) );
        }

        _buildProgress = .75f;

        if (_win64Enabled)
        {
            _buildProgressText = "Building for Win64 ...";
            Repaint();

            Debug.Log( "win64" +
                BuildPipeline.BuildPlayer(
                levels, _buildSavePath + "/Build V" + PlayerSettings.bundleVersion + "/" + _projectName + " win64 " + PlayerSettings.bundleVersion + "/" + _projectName + ".exe",
                BuildTarget.StandaloneWindows64, _devEnabled ? BuildOptions.Development : BuildOptions.None
            ) );
        }
    }

    private void ShowInExplorer()
    {
        Application.OpenURL( _buildSavePath );
    }

    #endregion
}
