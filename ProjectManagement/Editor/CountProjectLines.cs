﻿using System.Collections;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Shadowsun.Tools
{
    public class CountProjectLines : EditorWindow
    {
        private StringBuilder _strStats;
        private Vector2 _scrollPosition = new Vector2( 0, 0 );

        struct File
        {
            public string name;
            public int nbLines;

            public File(string name, int nbLines)
            {
                this.name = name;
                this.nbLines = nbLines;
            }
        }

        void OnGUI()
        {
            if (GUILayout.Button( "Refresh" ))
            {
                DoCountLines();
            }
            _scrollPosition = EditorGUILayout.BeginScrollView( _scrollPosition );
            {
                EditorGUILayout.HelpBox( _strStats.ToString(), MessageType.None );
            }
            EditorGUILayout.EndScrollView();
        }


        [MenuItem( "Tools/Shadowsun/Stats/Count Lines", false, 501 )]
        public static void Init()
        {
            CountProjectLines window = EditorWindow.GetWindow<CountProjectLines>( "Count Lines" );
            window.Show();
            window.Focus();
            window.DoCountLines();
        }

        void DoCountLines()
        {
            string strDir = System.IO.Directory.GetCurrentDirectory();
            strDir += @"/Assets/_Project/_Production/Scripts";
            int iLengthOfRootPath = strDir.Length;
            ArrayList statsAll = new ArrayList();
            ProcessDirectory( statsAll, strDir );

            int iTotalNbLines = 0;
            foreach (File f in statsAll)
            {
                iTotalNbLines += f.nbLines;
            }

            _strStats = new System.Text.StringBuilder();
            _strStats.Append( "Number of Files: " + statsAll.Count + "\n" );
            _strStats.Append( "Number of Lines: " + iTotalNbLines + "\n" );
            _strStats.Append( "================\n" );

            strDir = System.IO.Directory.GetCurrentDirectory();
            strDir += @"/Assets/_Project/_Production/Scripts";
            iLengthOfRootPath = strDir.Length;

            foreach (File f in statsAll)
            {
                _strStats.Append( f.name.Substring( iLengthOfRootPath + 1, f.name.Length - iLengthOfRootPath - 1 ) + " --> " + f.nbLines + "\n" );
            }
        }

        static void ProcessDirectory(ArrayList stats, string dir)
        {
            string[] strArrFiles = System.IO.Directory.GetFiles( dir, "*.cs" );
            foreach (string strFileName in strArrFiles)
                ProcessFile( stats, strFileName );

            strArrFiles = System.IO.Directory.GetFiles( dir, "*.js" );
            foreach (string strFileName in strArrFiles)
                ProcessFile( stats, strFileName );

            string[] strArrSubDir = System.IO.Directory.GetDirectories( dir );
            foreach (string strSubDir in strArrSubDir)
                ProcessDirectory( stats, strSubDir );
        }

        static void ProcessFile(ArrayList stats, string filename)
        {
            System.IO.StreamReader reader = System.IO.File.OpenText( filename );
            int iLineCount = 0;
            while (reader.Peek() >= 0)
            {
                reader.ReadLine();
                ++iLineCount;
            }
            stats.Add( new File( filename, iLineCount ) );
            reader.Close();
        }
    }
}