﻿using UnityEditor;

[InitializeOnLoad]
[CanEditMultipleObjects]
[CustomEditor( typeof( UnityEngine.Object ), true )]
public class UnityObjectEditor : Editor
{
}