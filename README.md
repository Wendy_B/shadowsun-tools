# Shadowsun Tool Suite
*This project contain a collection of my personnal tools, some public tools and modification of other commonly used tools.*
*When I use code or modify code that's not mine, I always add comments with a link to it's source.*
*Those scripts are for my personnal use and/or my team use when we are on production, you can use it if you want, you can address me any problem you have encountered, but **I can't and won't** make a technical support nor updates on demand.*

## CustomAttributes

coming soon


## CustomAttributes

Contains different Custom Attributes I use to customise my inspector.

- ButtonAttribute : Allow to add button for debug purpose in the inspector
- HelpBoxAttribvute : Allow to add helpbox in inspector with Info-warning-error icons (or none)
- HideAttribute : Allow to completely Hide a public or SerializedField variable
- IndentationAttribute : Allow to quickly make some Indentation on fields in Inspector (doesn't work with some other Attributes)
- ReadOnlyAttribute : The concerned public or SerializedField won't be able to be modified in inspector but is still readable
- SceneSelectorAttribute : on a string, allow to choose scene (from build settings) from a dropdown menu
- TagSelectorAttribute : on a string, allow to choose tag from a dropdown menu


## CustomGizmos

Contain different custom gizmos or utilities to draw gizmos.

- DrawArrowGizmo : Static functions to draw arrow gizmos 
- ShowDirection : Utility script to draw an arrow gizmo in a defined direction with a defined color
- DrawAllDirection : Utility that draw a 3 directional basic arrow gizmo

## CustomScriptablesObjects

Editor scripts that allow quick view and modification of scriptable objects in inspector SO fields.

## CustomTransform

Collection of editor scripts for my custom Transform.
Still has a few bugs with uniform scaling and when passing from 2D to 3D transform.

*Image coming soon*

## ProjectManagement

Collection of tools used in production.

- BuildManager : WIP tool that centralise all datas for build, also contain a version system and allow to multi platform Build
- ClearPlayerPrefs : little utility script to clear player prefs
- CountProjectLine : WIP utility to show how the project grow
- [HierarchyGroupHeader](http://diegogiacomelli.com.br/unitytips-hierarchy-window-group-header) : allow to make headers in hierarchy
- TakeScreenshot : shortcut to take game screenshot at any time with shift + F2

## QuickProjectSetup

Tool that deploy the whole project structure and the collection of utility scripts



*Will continue to update it and the description ASAP*






