﻿using UnityEngine;

[AddComponentMenu( "Gizmos/ShowDirection" )]
public class ShowDirection : MonoBehaviour
{
    public enum e_direction
    {
        Forward, 
        Backward, 
        Right,
        Left,
        Up,
        Down
    }

    public bool m_hideGizmo = false;
    public e_direction m_direction;
    public Color m_gizmoColor = Color.red;
    public float m_arrowSize = .5f;

    public void InitGizmo( e_direction dir, Color col, float size )
    {
        m_direction = dir;
        m_gizmoColor = col;
        m_arrowSize = size;
    }

    private void OnDrawGizmos()
    {
        if (!m_hideGizmo)
        {
            Gizmos.color = m_gizmoColor;

            Vector3 pos = GetComponent<Transform>().position;
            Vector3 rotation = GetComponent<Transform>().rotation.eulerAngles;
            Vector3 direction;

            switch (m_direction)
            {
                case e_direction.Forward:
                    direction = Quaternion.Euler( rotation ) * Vector3.forward;
                    break;
                case e_direction.Backward:
                    direction = Quaternion.Euler( rotation ) * Vector3.back;
                    break;
                case e_direction.Right:
                    direction = Quaternion.Euler( rotation ) * Vector3.right;
                    break;
                case e_direction.Left:
                    direction = Quaternion.Euler( rotation ) * Vector3.left;
                    break;
                case e_direction.Up:
                    direction = Quaternion.Euler( rotation ) * Vector3.up;
                    break;
                case e_direction.Down:
                    direction = Quaternion.Euler( rotation ) * Vector3.down;
                    break;
                default:
                    goto case e_direction.Forward;
            }

            DrawArrowGizmo.ForDebug( pos, direction * m_arrowSize, Gizmos.color, 0f, .2f * m_arrowSize, 25f );
        }
    }
}
