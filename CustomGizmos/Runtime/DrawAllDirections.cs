﻿using UnityEngine;

[AddComponentMenu( "Gizmos/DrawAllDirections" )]
public class DrawAllDirections : MonoBehaviour
{
    public bool m_hideGizmo = false;

    public float m_arrowSize = .5f;

    private void OnDrawGizmos()
    {
        if (!m_hideGizmo)
        {
            Vector3 pos = GetComponent<Transform>().position;
            Vector3 rotation = GetComponent<Transform>().rotation.eulerAngles;
            Vector3 direction;

            direction = Quaternion.Euler( rotation ) * Vector3.right;
            Gizmos.color = Color.red;

            DrawArrowGizmo.ForDebug( pos, direction * m_arrowSize, Gizmos.color, 0f, .2f * m_arrowSize, 25f );

            direction = Quaternion.Euler( rotation ) * Vector3.up;
            Gizmos.color = Color.green;

            DrawArrowGizmo.ForDebug( pos, direction * m_arrowSize, Gizmos.color, 0f, .2f * m_arrowSize, 25f );
            
            direction = Quaternion.Euler( rotation ) * Vector3.forward;
            Gizmos.color = Color.blue;

            DrawArrowGizmo.ForDebug( pos, direction * m_arrowSize, Gizmos.color, 0f, .2f * m_arrowSize, 25f );


        }
    }
}
