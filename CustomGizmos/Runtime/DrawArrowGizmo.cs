﻿using UnityEngine;
// adapted from http://wiki.unity3d.com/index.php/DrawArrow 

public static class DrawArrowGizmo
{
    public static void ForGizmo(Vector3 pos, Vector3 direction, Color? color = null, bool doubled = false, float arrowHeadLength = 0.2f, float arrowHeadAngle = 20.0f)
    {
        Gizmos.color = color ?? Color.white;

        //arrow shaft
        Gizmos.DrawRay( pos, direction );

        if (direction != Vector3.zero)
        {
            //arrow head
            Vector3 right = Quaternion.LookRotation( direction ) * Quaternion.Euler( 0, 180 + arrowHeadAngle, 0 ) * new Vector3( 0, 0, 1 );
            Vector3 left = Quaternion.LookRotation( direction ) * Quaternion.Euler( 0, 180 - arrowHeadAngle, 0 ) * new Vector3( 0, 0, 1 );
            Gizmos.DrawRay( pos + direction, right * arrowHeadLength );
            Gizmos.DrawRay( pos + direction, left * arrowHeadLength );
        }
    }

    public static void ForDebug(Vector3 pos, Vector3 direction, Color? color = null, float duration = 0.5f, float arrowHeadLength = 0.2f, float arrowHeadAngle = 30.0f)
    {
        Color actualColor = color ?? Color.white;
        duration = duration / Time.timeScale;

        Vector3 directlyRight = Vector3.zero;
        Vector3 directlyLeft = Vector3.zero;
        Vector3 directlyBack = Vector3.zero;
        Vector3 headRight = Vector3.zero;
        Vector3 headLeft = Vector3.zero;

        if (direction != Vector3.zero)
        {
            directlyRight = Quaternion.LookRotation( direction ) * Quaternion.Euler( 0, 180 + 90, 0 ) * new Vector3( 0, 0, 1 );
            directlyLeft = Quaternion.LookRotation( direction ) * Quaternion.Euler( 0, 180 - 90, 0 ) * new Vector3( 0, 0, 1 );
            directlyBack = Quaternion.LookRotation( direction ) * Quaternion.Euler( 0, 180, 0 ) * new Vector3( 0, 0, 1 );
            headRight = Quaternion.LookRotation( direction ) * Quaternion.Euler( 0, 180 + arrowHeadAngle, 0 ) * new Vector3( 0, 0, 1 );
            headLeft = Quaternion.LookRotation( direction ) * Quaternion.Euler( 0, 180 - arrowHeadAngle, 0 ) * new Vector3( 0, 0, 1 );
        }

        Debug.DrawRay( pos, direction, actualColor, duration ); //draw center line                                                      
        DrawArrowEnd( false, pos, direction, actualColor, arrowHeadLength, arrowHeadAngle, 1 ); //draw arrow head 
    }

    private static void DrawArrowEnd(bool gizmos, Vector3 pos, Vector3 direction, Color color, float arrowHeadLength = 0.25f, float arrowHeadAngle = 20.0f, float arrowPosition = 0.5f)
    {
        Vector3 right = ( Quaternion.LookRotation( direction ) * Quaternion.Euler( arrowHeadAngle, 0, 0 ) * Vector3.back ) * arrowHeadLength;
        Vector3 left = ( Quaternion.LookRotation( direction ) * Quaternion.Euler( -arrowHeadAngle, 0, 0 ) * Vector3.back ) * arrowHeadLength;
        Vector3 up = ( Quaternion.LookRotation( direction ) * Quaternion.Euler( 0, arrowHeadAngle, 0 ) * Vector3.back ) * arrowHeadLength;
        Vector3 down = ( Quaternion.LookRotation( direction ) * Quaternion.Euler( 0, -arrowHeadAngle, 0 ) * Vector3.back ) * arrowHeadLength;

        Vector3 arrowTip = pos + ( direction * arrowPosition );

        if (gizmos)
        {
            Gizmos.color = color;
            Gizmos.DrawRay( arrowTip, right );
            Gizmos.DrawRay( arrowTip, left );
            Gizmos.DrawRay( arrowTip, up );
            Gizmos.DrawRay( arrowTip, down );
        }
        else
        {
            Debug.DrawRay( arrowTip, right, color );
            Debug.DrawRay( arrowTip, left, color );
            Debug.DrawRay( arrowTip, up, color );
            Debug.DrawRay( arrowTip, down, color );
        }
    }
}