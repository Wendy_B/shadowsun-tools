﻿using System;
using UnityEngine;

[AttributeUsage( AttributeTargets.Field, Inherited = true, AllowMultiple = true )]
public class HelpBoxAttribute : PropertyAttribute
{
    public enum HelpBoxMessageType { None, Info, Warning, Error }

    public readonly string message;
    public readonly HelpBoxMessageType type;

    public HelpBoxAttribute(string message, HelpBoxMessageType type = HelpBoxMessageType.None)
    {
        this.message = message;
        this.type = type;
    }
}
