﻿using System;
using UnityEngine;

[AttributeUsage( AttributeTargets.Field, Inherited = true )]
public class IndentationAttribute : PropertyAttribute
{
    public readonly int level;
    public readonly bool readOnly;

    public IndentationAttribute()
    {
        this.level = 1;
    }

    public IndentationAttribute(int lvl)
    {
        this.level = lvl;
    }

    public IndentationAttribute(bool readOnly)
    {
        this.level = 1;
        this.readOnly = readOnly;
    }

    public IndentationAttribute(int lvl, bool readOnly)
    {
        this.level = lvl;
        this.readOnly = readOnly;
    }
}

[AttributeUsage( AttributeTargets.Field, Inherited = true, AllowMultiple = true )]
public class StartIndentationAttribute : PropertyAttribute
{

}

[AttributeUsage( AttributeTargets.Field, Inherited = true, AllowMultiple = true )]
public class EndIndentationAttribute : PropertyAttribute
{

}