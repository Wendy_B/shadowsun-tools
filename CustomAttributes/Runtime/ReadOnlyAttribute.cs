﻿using UnityEngine;

public class ReadOnlyAttribute : PropertyAttribute
{

}

public class StartReadOnly : PropertyAttribute
{

}

public class EndReadOnly : PropertyAttribute
{

}
