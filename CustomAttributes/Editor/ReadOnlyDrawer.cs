﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer( typeof( ReadOnlyAttribute ) )]
public class ReadOnlyDrawer : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property,
                                            GUIContent label)
    {
        return EditorGUI.GetPropertyHeight( property, label, true );
    }

    public override void OnGUI(Rect position,
                               SerializedProperty property,
                               GUIContent label)
    {
        GUI.enabled = false;
        EditorGUI.PropertyField( position, property, label, true );
        GUI.enabled = true;
    }
}

[CustomPropertyDrawer( typeof( StartReadOnly ) )]
public class StartReadOnlyDrawer : DecoratorDrawer
{
    public override float GetHeight()
    {
        return base.GetHeight();
    }

    public override void OnGUI(Rect position)
    {
        GUI.enabled = false;
    }
}

[CustomPropertyDrawer( typeof( EndReadOnly ) )]
public class EndReadOnlyDrawer : DecoratorDrawer
{
    public override float GetHeight()
    {
        return base.GetHeight();
    }

    public override void OnGUI(Rect position)
    {
        GUI.enabled = true;
    }
}