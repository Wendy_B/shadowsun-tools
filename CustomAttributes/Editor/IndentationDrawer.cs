﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer( typeof( IndentationAttribute ) )]
public class IndentationDrawer : PropertyDrawer
{

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return EditorGUI.GetPropertyHeight( property, label, true );
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {

        IndentationAttribute indent = attribute as IndentationAttribute;

        EditorGUI.indentLevel += indent.level;
        {
            if (indent.readOnly) GUI.enabled = false;
            {
                EditorGUI.PropertyField( position, property, label, true );
            }
            if (indent.readOnly) GUI.enabled = true;
        }
        EditorGUI.indentLevel -= indent.level;

    }
}

[CustomPropertyDrawer( typeof( StartIndentationAttribute ) )]
public class StartIndentationDrawer : DecoratorDrawer
{
    public override float GetHeight()
    {
        return base.GetHeight();
    }

    public override void OnGUI(Rect position)
    {
        EditorGUI.indentLevel++;
    }
}

[CustomPropertyDrawer( typeof( EndIndentationAttribute ) )]
public class EndIndentationDrawer : DecoratorDrawer
{
    public override float GetHeight()
    {
        return base.GetHeight();
    }

    public override void OnGUI(Rect position)
    {
        EditorGUI.indentLevel--;
    }
}

