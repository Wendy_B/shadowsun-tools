﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer( typeof( HelpBoxAttribute ) )]
public class HelpBoxDrawer : DecoratorDrawer
{
    public override float GetHeight()
    {
        var helpBoxAttribute = attribute as HelpBoxAttribute;
        if (helpBoxAttribute == null) return base.GetHeight();
        var helpBoxStyle = ( GUI.skin != null ) ? GUI.skin.GetStyle( "helpbox" ) : null;
        if (helpBoxStyle == null) return base.GetHeight();
        return Mathf.Max( 40f, helpBoxStyle.CalcHeight( new GUIContent( helpBoxAttribute.message ), EditorGUIUtility.currentViewWidth ) + 4 );
    }

    public override void OnGUI(Rect position)
    {
        HelpBoxAttribute helpBox = attribute as HelpBoxAttribute;
        if (helpBox == null) return;

        Rect rect = position;
        rect.x += EditorGUI.indentLevel * 15;
        rect.width -= EditorGUI.indentLevel * 15;

        EditorGUI.HelpBox( rect, helpBox.message, GetMessageType( helpBox.type ) );
    }

    private MessageType GetMessageType(HelpBoxAttribute.HelpBoxMessageType helpBoxMessageType)
    {
        switch (helpBoxMessageType)
        {
            default:
            case HelpBoxAttribute.HelpBoxMessageType.None: return MessageType.None;
            case HelpBoxAttribute.HelpBoxMessageType.Info: return MessageType.Info;
            case HelpBoxAttribute.HelpBoxMessageType.Warning: return MessageType.Warning;
            case HelpBoxAttribute.HelpBoxMessageType.Error: return MessageType.Error;
        }
    }
}
