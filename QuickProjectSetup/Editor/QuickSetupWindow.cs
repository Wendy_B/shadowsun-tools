﻿using System.IO;
using UnityEditor;
using UnityEngine;

namespace Shadowsun.Tools
{
    public class QuickSetupWindow
    {
        private static string projectPath = Application.dataPath + "/_Project";
        private static string localPath = "Assets/ShadowsunTools/QuickProjectSetup";
        private static string packagePath = "Packages/com.shadowsun.tools/QuickProjectSetup";

        [MenuItem( "Tools/Shadowsun/Project/Quick Setup", false, 1 )]
        private static void QuickProjectSetup()
        {
            ImportPackage(false);
        }

        private static void ImportPackage(bool validate = true)
        {
            if (Directory.Exists( packagePath ))
            {
                AssetDatabase.ImportPackage( packagePath + "/QuickSetup.unitypackage", validate );
                return;
            }

            if (Directory.Exists( localPath ))
            {
                AssetDatabase.ImportPackage( localPath + "/QuickSetup.unitypackage", validate );
                return;
            }
        }
    }
}

