﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Shadowsun.BitBox
{
    [RequireComponent( typeof(AudioSource) )]
    public class AudioSourceAnalyser : MonoBehaviour
    {
        #region Variables

        private AudioSource _as;

        // FTT values
        [HideInInspector] public float[] m_samplesLeft, m_samplesRight;

        // audio 8
        private float[] _freqBand8 = new float[8];
        private float[] _bandBuffer8 = new float[8];
        private float[] _bufferDecrease8 = new float[8];
        private float[] _freqBandHighest8 = new float[8];

        // audio band values
        [HideInInspector] public float[] m_audioBand8, m_audioBandBuffer8;

        // audio 64
        private float[] _freqBand64 = new float[64];
        private float[] _bandBuffer64 = new float[64];
        private float[] _bufferDecrease64 = new float[64];
        private float[] _freqBandHighest64 = new float[64];

        // audio band values
        [HideInInspector] public float[] m_audioBand64, m_audioBandBuffer64;

        // Amplitude variables
        [HideInInspector] public float m_amplitude, m_amplitudeBuffer;
        private float _amplitudeHighest;

        //  audio profile
        public float m_audioProfile;

        // stereo channels
        public enum e_channel
        {
            Stereo,
            Left,
            Right
        }

        public e_channel m_channel;

        #endregion

        #region System

        void Start()
        {
            m_samplesLeft = new float[512];
            m_samplesRight = new float[512];
            // init 8 audio band
            m_audioBand8 = new float[8];
            m_audioBandBuffer8 = new float[8];
            // init 64 audio band
            m_audioBand64 = new float[64];
            m_audioBandBuffer64 = new float[64];

            _as = GetComponent<AudioSource>();

            AudioProfile( m_audioProfile );
        }

        void Update()
        {
            if ( _as.clip != null )
            {
                GetSpectrumAudioSource();

                MakeFrequencyBands8();
                MakeFrequencyBands64();

                BandBuffer8();
                BandBuffer64();

                AudioBands8();
                AudioBands64();

                GetAmplitude();
            }
        }

        #endregion

        #region Methods

        private void AudioProfile( float audioProfile )
        {
            for ( int i = 0; i < 8; i++ )
            {
                _freqBandHighest8[i] = audioProfile;
            }
        }

        private void GetAmplitude()
        {
            float currentAmplitude = 0;
            float currentAmplitudeBuffer = 0;

            for ( int i = 0; i < 8; i++ )
            {
                currentAmplitude += m_audioBand8[i];
                currentAmplitudeBuffer += m_audioBandBuffer8[i];
            }

            if ( currentAmplitude > _amplitudeHighest )
            {
                _amplitudeHighest = currentAmplitude;
            }

            m_amplitude = currentAmplitude / _amplitudeHighest;
            m_amplitudeBuffer = currentAmplitudeBuffer / _amplitudeHighest;
        }


        private void GetSpectrumAudioSource()
        {
            // 0 for Left channel and 1 for Right
            _as.GetSpectrumData( m_samplesLeft, 0, FFTWindow.Blackman );
            _as.GetSpectrumData( m_samplesRight, 0, FFTWindow.Blackman );
        }

        private void AudioBands8()
        {
            for ( int i = 0; i < 8; i++ )
            {
                if ( _freqBand8[i] > _freqBandHighest8[i] )
                {
                    _freqBandHighest8[i] = _freqBand8[i];
                }

                m_audioBand8[i] = ( _freqBand8[i] / _freqBandHighest8[i] );
                m_audioBandBuffer8[i] = ( _bandBuffer8[i] / _freqBandHighest8[i] );
            }
        }

        private void AudioBands64()
        {
            for ( int i = 0; i < 64; i++ )
            {
                if ( _freqBand64[i] > _freqBandHighest64[i] )
                {
                    _freqBandHighest64[i] = _freqBand64[i];
                }

                m_audioBand64[i] = ( _freqBand64[i] / _freqBandHighest64[i] );
                m_audioBandBuffer64[i] = ( _bandBuffer64[i] / _freqBandHighest64[i] );
            }
        }


        private void BandBuffer8()
        {
            for ( int i = 0; i < 8; i++ )
            {
                if ( _freqBand8[i] > _bandBuffer8[i] )
                {
                    _bandBuffer8[i] = _freqBand8[i];
                    _bufferDecrease8[i] = 0.005f;
                }
                else if ( _freqBand8[i] < _bandBuffer8[i] )
                {
                    _bandBuffer8[i] -= _bufferDecrease8[i];
                    _bufferDecrease8[i] *= 1.2f;
                }
            }
        }

        private void BandBuffer64()
        {
            for ( int i = 0; i < 64; i++ )
            {
                if ( _freqBand64[i] > _bandBuffer64[i] )
                {
                    _bandBuffer64[i] = _freqBand64[i];
                    _bufferDecrease64[i] = 0.005f;
                }
                else if ( _freqBand64[i] < _bandBuffer64[i] )
                {
                    _bandBuffer64[i] -= _bufferDecrease64[i];
                    _bufferDecrease64[i] *= 1.2f;
                }
            }
        }

        private void MakeFrequencyBands8()
        {
            /*
             * 22050 / 512 = 43 hertz
             * 
             * 20 - 60 hertz
             * 60 - 250 hertz
             * 250 - 500 hertz
             * 500 - 2000 hertz
             * 2000 - 4000 hertz
             * 4000 - 6000 hertz
             * 6000 - 20000 hertz
             * ------------------------------------- 
             * 0 -   2 =    86 hertz
             * 1 -   4 =   172 hertz -    87 -   258
             * 2 -   8 =   344 hertz -   259 -   602
             * 3 -  16 =   688 hertz -   603 -  1290
             * 4 -  32 =  1376 hertz -  1291 -  2666
             * 5 -  64 =  2752 hertz -  2667 -  5418
             * 6 - 128 =  5504 hertz -  5419 - 10922
             * 7 - 256 = 11008 hertz - 10923 - 21930
             * -------------------------------------
             *     510
             */

            int count = 0;

            for ( int i = 0; i < 8; i++ )
            {
                float average = 0f;
                int sampleCount = (int) Mathf.Pow( 2, i ) * 2;

                if ( i == 7 )
                {
                    sampleCount += 2; // so we reach the full spectrum : 512
                }

                for ( int j = 0; j < sampleCount; j++ )
                {
                    switch ( m_channel )
                    {
                        case e_channel.Stereo:
                            average += ( m_samplesLeft[count] + m_samplesRight[count] ) * ( count + 1 );
                            break;
                        case e_channel.Left:
                            average += m_samplesLeft[count] * ( count + 1 );
                            break;
                        case e_channel.Right:
                            average += m_samplesLeft[count] * ( count + 1 );
                            break;
                        default:
                            break;
                    }

                    count++;
                }

                average /= count;
                _freqBand8[i] = average * 10;
            }
        }

        private void MakeFrequencyBands64()
        {
            /*
             *  0-15 =  1 sample  = 16
             * 16-31 =  2 samples = 32
             * 32-39 =  4 samples = 32
             * 40-47 =  6 samples = 48
             * 48-55 = 16 samples = 128
             * 56-63 = 32 samples = 256
             *                      ---
             *                      512
             * 
             */

            int count = 0;
            int sampleCount = 1;
            int power = 0;

            for ( int i = 0; i < 64; i++ )
            {
                float average = 0f;

                if ( i == 16 || i == 32 || i == 40 || i == 48 || i == 56 )
                {
                    power++;
                    sampleCount = (int) Mathf.Pow( 2, power );

                    if ( power == 3 )
                    {
                        sampleCount -= 2;
                    }
                }

                for ( int j = 0; j < sampleCount; j++ )
                {
                    switch ( m_channel )
                    {
                        case e_channel.Stereo:
                            average += ( m_samplesLeft[count] + m_samplesRight[count] ) * ( count + 1 );
                            break;
                        case e_channel.Left:
                            average += m_samplesLeft[count] * ( count + 1 );
                            break;
                        case e_channel.Right:
                            average += m_samplesLeft[count] * ( count + 1 );
                            break;
                        default:
                            break;
                    }

                    count++;
                }

                average /= count;
                _freqBand64[i] = average * 10;
            }
        }

        #endregion
    }
}